<?php
    date_default_timezone_set('Europe/Prague')
?>

<!DOCTYPE html>
<html lang="cs">
<head>
    <title>Teploty</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: sans-serif;
            display: flex;
            flex-direction: column;
            background-color: white;
        }

        table {
            text-align: right;
            width: 300px;
            margin: 0 auto;
        }

        table td, table th {
            border-bottom: 1px solid black;
            padding: 5px 15px 5px 5px;
        }

        #logo {
            margin: 40px auto 0 auto;
            width: 300px;

        }

        #logo img {
            filter: brightness(0%);
        }

        #date {
            margin: 10px auto;
            width: 300px;
            position:relative;
        }

        #date form {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        #date label {
            font-size: 16px;
            margin-left: 15px;
        }

        #date #start {
            width: 170px;
            font-size: 16px;
        }

        #date #submit {
            height: 24px;
            width: 48px;
            font-size: 16px;
        }

        @media print {
            #date {
                display: none;
            }
        }

    </style>
</head>
<body>
<div id="logo">
    <img width="300" src="https://cdn.myshoptet.com/usr/www.ruzovadistribuce.cz/user/logos/logoruzova2.png"
         alt="Ruzovadistribuce.cz">
</div>
<div id="date">
    <form action="" method="POST">
        <label for="start">Datum:</label>
        <input
                type="datetime-local"
                id="start"
                name="time-start"
                value="<?php echo !empty($_POST['time-start']) ? $_POST["time-start"] : date("Y-m-d\TH:i"); ?>"
                min="2024-04-01T00:00"
                max="<?php echo date("Y-m-d\TH:i"); ?>"
        />
        <input id="submit" type="submit" value="OK">
    </form>
</div>
<table>
    <thead>
    <tr>
        <th>Datum</th>
        <th>Záznam teploty</th>
    </tr>
    </thead>
    <tbody>
    <?php
    function parseTemperatureFile($year)
    {
        $csv = [];
        if (($handle = fopen("./temperatures/temperatures-".$year.".csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $csv[$data[0] . "-" . $data[1]] = $data[2];
            }
            fclose($handle);

        }
        return $csv;
    }

    $date = strtotime("now");

    if (!empty($_POST['time-start'])) {
        $date = strtotime($_POST["time-start"]);
    }

    $dates = parseTemperatureFile(date("Y", $date));
    $minutes = date("i", $date);
    $date = $date - (($minutes % 15) * 60);


    for ($i = 0; $i < 16; $i++) {
        $curTime = $date - ($i * 15 * 60);
        $curDate = date("d/m/Y-H:i", $curTime);
        $curTimePrint = date("d/m/Y H:i", $curTime);
        echo '<tr><td>' . $curTimePrint . '</td><td>' . $dates[$curDate] . ' °C</td></tr>';
    }

    ?>
    </tbody>
</table>
</body>
</html>