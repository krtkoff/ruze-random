const fs = require('node:fs');

console.log("Start generating ...");

let now = new Date();
let startDate = new Date("2024-04-01");
let endDate = new Date("2050-01-01");

let temperatures = "./temperatures/temperatures";
let previous = 5.3;
let temperature = 5.2;
let direction = "down"

console.log("Start: ", startDate);
console.log("End: ", endDate);
console.log("Now: ", `${now.getDate()}/${now.getMonth() + 1}/${now.getFullYear()} , ${now.getHours()}:${now.getMinutes()}`);

startDate.setHours(startDate.getHours() - 2);
endDate.setHours(endDate.getHours() - 2);

createText(`${temperatures}-${startDate.getFullYear()}.csv`,`Date,Time,Temperature\n`)

for (let currentDate = startDate ; currentDate < endDate ; currentDate.setMinutes(currentDate.getMinutes() + 15)) {
  let day = (currentDate.getDate() < 10 ? '0' : '') + currentDate.getDate();
  let month = ((currentDate.getMonth() + 1) < 10 ? '0' : '') + (currentDate.getMonth() + 1);
  let hours = ((currentDate.getHours()) < 10 ? '0' : '') + (currentDate.getHours());
  let minutes = (currentDate.getMinutes() < 10 ? '0' : '') + currentDate.getMinutes()
  let date = `${day}/${month}/${currentDate.getFullYear()}`
  let time = `${hours}:${minutes}`


  temperature = parseFloat(getTemperature(previous, direction).toFixed(2));
  direction = previous < temperature ? "up" : "down"
  previous = temperature;

  appendText(`${temperatures}-${currentDate.getFullYear()}.csv`,`${date},${time},${temperature.toFixed(1)}\n`);
}


function getTemperature(previous, direction) {
  let randomDirection = Math.round((Math.random() * 100) + Number.EPSILON);
  let move = Math.round((Math.random() * 20) + Number.EPSILON) / 100;

  // console.log(`${previous},${direction},${randomDirection},${move}`)

  if (previous + move > 6) {
   // console.log(`${previous},${direction},${randomDirection},${move}`)
  }

  if (direction === "up") {
    if (previous + move > 6) { return previous - move }
    if (previous - move < 4) { return previous + move }
    return randomDirection <= 70 ? previous + move : previous - move;
  }
  else if (direction === "down") {
    if (previous + move > 6) { return previous - move }
    if (previous - move < 4) { return previous + move }
    return randomDirection <= 70 ? previous - move : previous + move;
  }
  else {
    return previous
  }
}

function createText(file, string) {
  fs.writeFile(file, string, { flag: "w" }, (err,file) => {
    if (err) {
      fs.close(file, err => console.log(err));
      console.error(err);
    } else {
      file && fs.close(file, err => console.log(err));
      return true;
    }
  });
}

function appendText(file, string) {
  fs.appendFileSync(file, string, (err,file) => {
    if (err) {
      fs.close(file, err => console.log(err));
      console.error(err);
    } else {
      fs.close(file, err => console.log(err));
      return true;
    }
  });
}